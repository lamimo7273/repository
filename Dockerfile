FROM ubuntu:latest
RUN export DEBIAN_FRONTEND=noninteractive && set DEBIAN_FRONTEND=noninteractive && apt-get -y update && apt-get -y install openssh-server python rsyslog less vim sudo tzdata nano neofetch
RUN echo 'root:root' | chpasswd
EXPOSE 22
ENTRYPOINT echo "Starting SSH Service!" && service ssh stop && service ssh start && echo "OK!" && echo "Connect via user root and password root" && echo "Starting bash for interactive use!" && bash